************
Installation
************

Using ``virtualenvwrapper``,

.. code-block:: bash

    $ git clone https://frnsroy@bitbucket.org/frnsroy/metal_semi.git
    $ cd metal_semi
    $ mkvirtualenv v_metal_semi
    $ pip install -e .

