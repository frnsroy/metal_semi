.. _usage-label:

*****
Usage
*****

Basic example:

.. code-block:: python
    :linenos:

    >>> from contact.barrier import Barrier
    >>> barrier = Barrier()
    >>> barrier.print_properties()
