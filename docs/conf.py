# -*- coding: utf-8 -*-
"""
doc.conf.py
September 11, 2018
@author Francois Roy
"""
import os
import sys
from unipath import Path


# In src dir type:
# sphinx-build -b html docs/. docs/html/_build/.

ROOT_DIR = Path(os.path.abspath(__file__)).ancestor(2)
sys.path.insert(0, ROOT_DIR)
sys.path.insert(1, ROOT_DIR.child('docs').child('images'))

# General information about the project.
project = u'metal_semi'
copyright = u'2016-2019, Francois Roy'
author = u'Francois Roy'
html_logo = 'images/logo.png'
html_favicon = 'images/icon.png'

extensions = [
    'sphinx.ext.mathjax',
    'sphinx.ext.autodoc',
    'sphinx.ext.viewcode',
    'matplotlib.sphinxext.plot_directive',
]

numfig = True
numfig_format = {'figure': 'Figure %s',
                 'table': 'Table %s',
                 'code-block': 'Listing %s',
                 'section': 'Section'}
templates_path = ['_templates']
html_static_path = ['_static']

html_show_sourcelink = False
source_suffix = ['.rst']
master_doc = 'index'
language = None
exclude_patterns = ['_build']
pygments_style = 'sphinx'
todo_include_todos = True
html_theme = 'alabaster'
html_theme_options = {'show_powered_by': False,
                      'page_width': '1200px',
                      'fixed_sidebar': True}
# add in each page
rst_prolog = """
.. role:: red
"""
html_sidebars = {
        '**': [
            'navigation.html',
            'localtoc.html',
            'searchbox.html',
    ]
}
