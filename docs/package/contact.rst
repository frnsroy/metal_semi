contact
=======


.. automodule:: contact.barrier
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: contact.pcfs
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: contact.wave
    :members:
    :undoc-members:
    :show-inheritance:
