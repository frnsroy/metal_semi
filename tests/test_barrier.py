# -*- coding: utf-8 -*-
"""
tests.test_barrier.py
January 22, 2019
@author Francois Roy
"""
import numpy as np
from contact.barrier import Barrier, EPS


class TestBarrier:
    r"""Test the Barrier class."""
    def test_default_values(self, barrier):
        r""""""
        # Default values
        inputs = {'Va': 0.0,
                  'Nd': 1e25,
                  'Na': 0.0,
                  'Phi': 4.75,
                  'Eg': 1.12,
                  'chi': 4.05,
                  'T': 300.0,
                  'epsilonrs': 11.7,
                  }
        for key, value in barrier.inputs.items():
            assert np.isclose(inputs[key], value, 1e-6)

    def test_arguments(self):
        r""""""
        Nd = 1e23
        b = Barrier(Nd=Nd)
        assert np.isclose(Nd, b.inputs['Nd'], 1e-6)

    def test_set_applied_voltage(self, barrier):
        r"""

        :param barrier: The barrier fixture --see
            :func:`barrier<tests.conftest.barrier()>`
        """
        Va = 0.1
        barrier.set_applied_voltage(Va)
        assert np.isclose(Va, barrier.inputs['Va'], 1e-6)

    def test_set_temperature(self, barrier):
        r"""

        :param barrier: The barrier fixture --see
            :func:`barrier<tests.conftest.barrier()>`
        """
        T = 301.0
        barrier.set_temperature(T)
        assert np.isclose(T, barrier.inputs['T'], 1e-6)

    def test_compute_gammas_boltzmann(self, barrier_boltzmann):
        r"""

        :param barrier_boltzmann: The barrier fixture --see
            :func:`barrier_boltzmann<tests.conftest.barrier_boltzmann()>`
        """
        gammas = barrier_boltzmann.compute_gammas()
        assert np.isclose(1.0, gammas[0], 1e-3)
        assert np.isclose(1.0, gammas[1], 1e-3)

    def test_compute_gammas(self, barrier):
        r"""

        :param barrier: The barrier fixture --see
            :func:`barrier<tests.conftest.barrier()>`
        """
        gammas = barrier.compute_gammas()
        assert np.isclose(0.8937305, gammas[0], 1e-6)
        assert np.isclose(1.0, gammas[1], 1e-6)

    def test_compute_gammas_ptype(self, barrier_ptype):
        r"""

        :param barrier_ptype: The barrier fixture --see
            :func:`barrier_ptype<tests.conftest.barrier_ptype()>`
        """
        gammas = barrier_ptype.compute_gammas()
        assert np.isclose(1.0, gammas[0], 1e-6)
        assert np.isclose(0.77138216, gammas[1], 1e-6)

    def test_init_values(self, barrier):
        r"""

        :param barrier: The barrier fixture --see
            :func:`barrier<tests.conftest.barrier()>`
        """
        values = {'lambda_D': 1.292883E-09,
                  'Vbi': 6.777390E-01,
                  'PhiB_e': 7.000000E-01,
                  'PhiB_h': 4.200000E-01,
                  'ua': 0.000000E+00,
                  'ubi': 2.621613E+01,
                  'eta_fs': 0.000000E+00,
                  'eta_veq': -4.246246E+01,
                  'Ev_eq': -1.097739E+00,
                  'Ec_eq': 2.226097E-02,
                  'Ef_I': 0.000000E+00,
                  'eta_g': 4.332355E+01,
                  'eta_fI': 0.000000E+00,
                  'Ndoping': 1.000000E+25,
                  'xb_h': 0.000000E+00,
                  'xb_e': 9.361778E-09,
                  'eta_be': 2.707722E+01,
                  'V_eq': -4.072261E+00,
                  'eta_ceq': 0.86109295E+00,
                  'Nc': 2.800000E+25,
                  'Ef_s': 0.000000E+00,
                  'p_eq': 4.456697E+06,
                  'Vth': 2.585199E-02,
                  'n_eq': 1.000000E+25,
                  'Nv': 1.040000E+25,
                  'eta_bh': 1.624633E+01,
                  'ni': 6.675850E+15,
                  }
        for key, value in barrier.values.items():
            assert np.isclose(values[key], value, 0.01*abs(values[key]))

    def test_p_type_values(self, barrier_ptype):
        r"""

        :param barrier_ptype: The barrier fixture --see
            :func:`barrier_ptype<tests.conftest.barrier_ptype()>`
        """
        n_eq = 4.456697E+06
        p_eq = 1.000000E+25
        V_eq = -5.17905172108E+00
        assert np.isclose(0.0, barrier_ptype.inputs['Nd'], 1e-10)
        assert np.isclose(1.0e25, barrier_ptype.inputs['Na'], 1.0)
        assert np.isclose(n_eq, barrier_ptype.values['n_eq'], 1.0)
        assert np.isclose(p_eq, barrier_ptype.values['p_eq'], 1.0)
        assert np.isclose(V_eq, barrier_ptype.values['V_eq'], 1e-4)

    def test_barrier_type(self):
        r""""""
        Va = 1.0  # Strong forward bias ua > ubi
        b = Barrier(Va=Va)
        xb_e = 0.0
        xb_h = 6.499006E-09
        assert np.isclose(b._inputs['Va'], Va, 1.0e-4)
        assert np.isclose(b.values['xb_e'], xb_e, 10*EPS)
        assert np.isclose(b.values['xb_h'], xb_h, 10*EPS)

    def test_electrons_barrier_levels(self):
        r""""""
        b = Barrier()
        num = 11
        levels = b.barrier_levels(num)
        xi = np.array([np.linspace(0, 1.0, num)])
        eta_ceq = np.array([27.07721814, 22.09615436, 17.63941307,
                             13.70699430, 10.29889802, 7.41512425,
                             5.05567298, 3.22054422, 1.90973796,
                             1.12325421,   0.86109295])
        eta_veq = np.array([-16.24633088, -21.22739467, -25.68413595,
                             -29.61655473, -33.02465100, -35.90842478,
                             -38.26787604, -40.10300481, -41.41381106,
                             -42.20029482, -42.46245607])
        assert np.allclose(xi[0], levels[0, :], EPS, EPS)
        assert np.allclose(eta_ceq, levels[1, :], 1e-6, 1e-6)
        assert np.allclose(eta_veq, levels[2, :], 1e-6, 1e-6)

    def test_holes_barrier_levels(self):
        Va = 1.0  # Strong forward bias ua > ubi
        b = Barrier(Va=Va)
        num = 11
        levels = b.barrier_levels(num)
        xi = np.array([np.linspace(0, 1.0, num)])
        eta_ceq = np.array([-11.60452206, -9.23605521, -7.11690066,
                            -5.2470584, -3.62652845, -2.2553108,
                            -1.13340545, -0.2608124, 0.36246835,
                            0.7364368, 0.86109295])
        eta_veq = np.array([-54.92807109, -52.55960423, -50.44044968,
                            -48.57060743, -46.95007748, -45.57885983,
                            -44.45695447, -43.58436142, -42.96108067,
                            -42.58711222, -42.46245607])
        assert np.allclose(xi[0], levels[0], EPS, EPS)
        assert np.allclose(eta_ceq, levels[1], 1e-6, 1e-6)
        assert np.allclose(eta_veq, levels[2], 1e-6, 1e-6)
