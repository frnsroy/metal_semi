import pytest
from contact.barrier import Barrier
from contact.wave import Wave
from contact.pcfs import PCFs


@pytest.fixture
def barrier():
    return Barrier()


@pytest.fixture
def barrier_ptype():
    return Barrier(Nd=0.0, Na=1.0e25)


@pytest.fixture
def barrier_boltzmann():
    return Barrier(Nd=1.0e22)


@pytest.fixture
def wave_e():
    b = Barrier()
    eta = 1.1*b.values['eta_ceq']
    return Wave(eta, b)


@pytest.fixture
def wave_h():
    b = Barrier()
    Vbi = b.values['Vbi']
    b.set_applied_voltage(1.1 * Vbi)
    eta = 1.1*b.values['eta_veq']
    return Wave(eta, b)


@pytest.fixture
def pcfs_00():
    a = 0.0
    z = 0.0
    return PCFs(a, z)


@pytest.fixture
def pcfs_z0():
    a = -5.0
    z = 0.0
    return PCFs(a, z)


@pytest.fixture
def pcfs_a0():
    a = 0.0
    z = 4.326135
    return PCFs(a, z)


@pytest.fixture
def pcfs_btp():
    """The particle energy is below the barrier."""
    a = -0.8
    z = 2.2*(abs(a))**0.5
    return PCFs(a, z)


@pytest.fixture
def pcfs_tp():
    """At the classical turning point."""
    a = -1.2
    z = 2*(abs(a))**0.5
    return PCFs(a, z)


@pytest.fixture
def pcfs_atp():
    """The particle energy is above the potential barrier."""
    a = -3.3
    z = 1.8*(abs(a))**0.5
    return PCFs(a, z)
