# -*- coding: utf-8 -*-
"""
tests.test_pcfs.py
January 22, 20189
@author Francois Roy
"""
import pytest
import numpy as np
import scipy.special as special
from contact.pcfs import PCFs

INPUTS = {}
EPS = np.finfo(float).eps

"""
REFERENCE: z = 4.326135, a varies between 0.0 to -10.0
REFERENCE[0] = U(a, 4.326135),
REFERENCE[1] = V(a, 4.326135),
REFERENCE[2] = Up(a, 4.326135),
REFERENCE[3] = Vp(a, 4.326135),
REFERENCE[4] = Upp(a, 4.326135),
REFERENCE[5] = Vpp(a, 4.326135)
"""
REFERENCE = [[+4.385484198E-03, +1.944507999E-02, +8.192929901E-02,
              +3.252695880E-01, +1.202336902E+00, +4.063028194E+00,
              +1.216669242E+01, +3.028809884E+01, +5.194690375E+01,
              -2.431422836E+00, -4.520673453E+02],
             [+4.225410045E+01, +1.083109695E+01, +3.068458106E+00,
              +9.773868232E-01, +3.313854966E-01, +1.013847938E-01,
              +1.949432879E-02, -2.623030112E-03, -4.112254822E-03,
              -1.784369341E-03, -3.797018777E-04],
             [-9.95898165E-03, -3.986827846E-02, -1.480509840E-01,
              -4.98756827E-01, -1.462292319E+00, -3.378088181E+00,
              -3.97072189E+00, +1.356829848E+01, +1.147960821E+02,
              +4.46808014E+02, +9.547536656E+02],
             [+8.59829230E+01, +1.882570665E+01, +4.193814945E+00,
              +9.54304434E-01, +2.605776257E-01, +1.120833442E-01,
              +5.92172448E-02, +2.516811998E-02, +6.272054643E-03,
              -2.52543492E-04, -9.630485495E-04],
             [+2.05190710E-02, +7.153574658E-02, +2.194772046E-01,
              +5.46082429E-01, +8.162196443E-01, -1.304796781E+00,
              -1.60738917E+01, -7.030288720E+01, -1.725228876E+02,
              +1.05065160E+01, +2.405513178E+03],
             [+1.97701063E+02, +3.984610020E+01, +8.219972790E+00,
              +1.64089663E+00, +2.249646920E-01, -3.255861080E-02,
              -2.57547178E-02, +6.088417470E-03, +1.365736982E-02,
              +7.71050793E-03, +2.020446465E-03]]


class TestPCFs:
    r"""Test the PCFs class."""
    def test_class_argument_a(self):
        r""""""
        with pytest.raises(ValueError) as e:
            PCFs(1j, 0.0)
        e.match(r'.* a must .*')

    def test_class_argument_z(self):
        r""""""
        with pytest.raises(ValueError) as e:
            PCFs(0.0, 1j)
        e.match(r'.* z must .*')

    def test_default_values(self, pcfs_00):
        r"""
        :param pcfs_00: The function fixture --see
            :func:`pcfs_00<tests.conftest.pcfs_00()>`
        """
        for key, value in pcfs_00.inputs.items():
            assert np.isclose(INPUTS[key], value, EPS)

    def test_set_a_real_negative(self, pcfs_a0):
        r"""
        :param pcfs_a0: The function fixture --see
            :func:`pcfs_a0<tests.conftest.pcfs_a0()>`
        """
        a = -5.0
        pcfs_a0.a = a
        assert np.isclose(a, pcfs_a0.a, EPS)

    def test_set_a_imaginary_positive(self, pcfs_a0):
        r"""Must raise an exception.

        :param pcfs_a0: The function fixture --see
            :func:`pcfs_a0<tests.conftest.pcfs_a0()>`
        """
        with pytest.raises(ValueError) as e:
            pcfs_a0.a = 1j
        e.match(r'.* a must .*')

    def test_set_z_real_positive(self, pcfs_z0):
        r"""
        :param pcfs_z0: The function fixture --see
            :func:`pcfs_z0<tests.conftest.pcfs_z0()>`
        """
        z = 1.0
        pcfs_z0.z = z
        assert np.isclose(z, pcfs_z0.z, EPS)

    def test_set_z_imaginary_positive(self, pcfs_z0):
        r"""Must raise an exception.
        :param pcfs_z0: The function fixture --see
            :func:`pcfs_z0<tests.conftest.pcfs_z0()>`
        """
        with pytest.raises(ValueError) as e:
            pcfs_z0.z = 1j
        e.match(r'.* z must .*')

    def test_compute_s_a0(self, pcfs_a0):
        r"""
        :param pcfs_a0: The function fixture --see
            :func:`pcfs_a0<tests.conftest.pcfs_a0()>`
        """
        S = pcfs_a0.compute_S()
        assert np.isclose(0.0, S, 1e-6, 1e-6)

    def test_compute_s_at_tp(self, pcfs_tp):
        r"""
        :param pcfs_tp: The function fixture --see
            :func:`pcfs_tp<tests.conftest.pcfs_tp()>`
        """
        S = pcfs_tp.compute_S()
        assert np.isclose(0.0, S, 1e-6, 1e-6)

    def test_compute_s_below_tp(self, pcfs_btp):

        r"""
        .. math:: 2*sqrt(|a|) < z
        :param pcfs_btp: The function fixture --see
            :func:`pcfs_btp<tests.conftest.pcfs_btp()>`
        """
        S = pcfs_btp.compute_S()
        assert np.isclose(4.8412057648021867E-02, S, 1e-6, 1e-6)

    def test_compute_s_above_tp(self, pcfs_atp):
        r"""
        .. math:: 2*sqrt(|a|) > z
        :param pcfs_atp: The function fixture --see
            :func:`pcfs_atp<tests.conftest.pcfs_atp()>`
        """
        S = pcfs_atp.compute_S()
        assert np.isclose(1.9379549269608565E-01, np.real(S), 1e-6, 1e-6)
        assert np.isclose(0.0, np.imag(S), 1e-6, 1e-6)

    def test_uint_at_tp(self, pcfs_tp):
        r"""
         :param pcfs_tp: The function fixture --see
             :func:`pcfs_tp<tests.conftest.pcfs_tp()>`
         """
        Uref = special.pbdv(-pcfs_tp.a-0.5, pcfs_tp.z)[0]
        Uint = pcfs_tp.compute_Uint()
        assert np.isclose(Uref, Uint, 2.0)

    def test_uint_below_tp(self, pcfs_btp):
        r"""
         :param pcfs_btp: The function fixture --see
             :func:`pcfs_btp<tests.conftest.pcfs_btp()>`
         """
        Uref = special.pbdv(-pcfs_btp.a-0.5, pcfs_btp.z)[0]
        Uint = pcfs_btp.compute_Uint()
        assert np.isclose(Uref, Uint, 0.1)

    def test_uint_above_tp(self, pcfs_atp):
        r"""
         :param pcfs_atp: The function fixture --see
             :func:`pcfs_atp<tests.conftest.pcfs_atp()>`
         """
        Uref = special.pbdv(-pcfs_atp.a-0.5, pcfs_atp.z)[0]
        Uint = pcfs_atp.compute_Uint()
        assert np.isclose(Uref, Uint, 0.1)

    def test_vint_at_tp(self, pcfs_tp):
        r"""
         :param pcfs_tp: The function fixture --see
             :func:`pcfs_tp<tests.conftest.pcfs_tp()>`
         """
        Vref = special.pbvv(-pcfs_tp.a-0.5, pcfs_tp.z)[0]
        Vint = pcfs_tp.compute_Vint()
        assert np.isclose(Vref, Vint, 2.0)

    def test_vint_below_tp(self, pcfs_btp):
        r"""
         :param pcfs_btp: The function fixture --see
             :func:`pcfs_btp<tests.conftest.pcfs_btp()>`
         """
        Vref = special.pbvv(-pcfs_btp.a-0.5, pcfs_btp.z)[0]
        Vint = pcfs_btp.compute_Vint()
        assert np.isclose(Vref, Vint, 0.1)

    def test_vint_above_tp(self, pcfs_atp):
        r"""
         :param pcfs_atp: The function fixture --see
             :func:`pcfs_atp<tests.conftest.pcfs_atp()>`
         """
        Vref = special.pbvv(-pcfs_atp.a-0.5, pcfs_atp.z)[0]
        Vint = pcfs_atp.compute_Vint()
        assert np.isclose(Vref, Vint, 0.1)

    def test_upint_at_tp(self, pcfs_tp):
        r"""
         :param pcfs_tp: The function fixture --see
             :func:`pcfs_tp<tests.conftest.pcfs_tp()>`
         """
        Upref = special.pbdv(-pcfs_tp.a-0.5, pcfs_tp.z)[1]
        Upint = pcfs_tp.compute_Upint()
        assert np.isclose(Upref, Upint, 2.0)

    def test_upint_below_tp(self, pcfs_btp):
        r"""
         :param pcfs_btp: The function fixture --see
             :func:`pcfs_btp<tests.conftest.pcfs_btp()>`
         """
        Upref = special.pbdv(-pcfs_btp.a-0.5, pcfs_btp.z)[1]
        Upint = pcfs_btp.compute_Upint()
        assert np.isclose(Upref, Upint, 0.1)

    def test_upint_above_tp(self, pcfs_atp):
        r"""
         :param pcfs_atp: The function fixture --see
             :func:`pcfs_atp<tests.conftest.pcfs_atp()>`
         """
        Upref = special.pbdv(-pcfs_atp.a-0.5, pcfs_atp.z)[1]
        Upint = pcfs_atp.compute_Upint()
        assert np.isclose(Upref, Upint, 0.1)

    def test_vpint_at_tp(self, pcfs_tp):
        r"""
         :param pcfs_tp: The function fixture --see
             :func:`pcfs_tp<tests.conftest.pcfs_tp()>`
         """
        Vpref = special.pbvv(-pcfs_tp.a-0.5, pcfs_tp.z)[1]
        Vpint = pcfs_tp.compute_Vpint()
        assert np.isclose(Vpref, Vpint, 2.0)

    def test_vpint_below_tp(self, pcfs_btp):
        r"""
         :param pcfs_btp: The function fixture --see
             :func:`pcfs_btp<tests.conftest.pcfs_btp()>`
         """
        Vpref = special.pbvv(-pcfs_btp.a-0.5, pcfs_btp.z)[1]
        Vpint = pcfs_btp.compute_Vpint()
        assert np.isclose(Vpref, Vpint, 0.1)

    def test_vpint_above_tp(self, pcfs_atp):
        r"""
         :param pcfs_atp: The function fixture --see
             :func:`pcfs_atp<tests.conftest.pcfs_atp()>`
         """
        Vpref = special.pbvv(-pcfs_atp.a-0.5, pcfs_atp.z)[1]
        Vpint = pcfs_atp.compute_Vpint()
        assert np.isclose(Vpref, Vpint, 0.1)

    # Test derivatives

    def test_compute_up_z0(self, pcfs_z0):
        r"""
         :param pcfs_z0: The function fixture --see
             :func:`pcfs_z0<tests.conftest.pcfs_z0()>`
         """
        Upref = special.pbdv(-pcfs_z0.a-0.5, pcfs_z0.z)[1]
        Up = pcfs_z0.compute_Up()
        assert np.isclose(Upref, Up, EPS)

    def test_compute_vp_z0(self, pcfs_z0):
        r"""
         :param pcfs_z0: The function fixture --see
             :func:`pcfs_z0<tests.conftest.pcfs_z0()>`
         """
        Vpref = special.pbvv(-pcfs_z0.a-0.5, pcfs_z0.z)[1]
        Vp = pcfs_z0.compute_Vp()
        assert np.isclose(Vpref, Vp, EPS)

    def test_compute_up_a0(self, pcfs_a0):
        r"""
         :param pcfs_a0: The function fixture --see
             :func:`pcfs_a0<tests.conftest.pcfs_a0()>`
         """
        Upref = special.pbdv(-pcfs_a0.a-0.5, pcfs_a0.z)[1]
        Up = pcfs_a0.compute_Up()
        assert np.isclose(Upref, Up, EPS)

    def test_compute_vp_a0(self, pcfs_a0):
        r"""
         :param pcfs_a0: The function fixture --see
             :func:`pcfs_a0<tests.conftest.pcfs_a0()>`
         """
        Vpref = special.pbvv(-pcfs_a0.a-0.5, pcfs_a0.z)[1]
        Vp = pcfs_a0.compute_Vp()
        assert np.isclose(Vpref, Vp, EPS)

    def test_compute_up_btp(self, pcfs_btp):
        r"""
         :param pcfs_btp: The function fixture --see
             :func:`pcfs_btp<tests.conftest.pcfs_btp()>`
         """
        Upref = special.pbdv(-pcfs_btp.a-0.5, pcfs_btp.z)[1]
        Up = pcfs_btp.compute_Up()
        assert np.isclose(Upref, Up, EPS)

    def test_compute_vp_btp(self, pcfs_btp):
        r"""
         :param pcfs_btp: The function fixture --see
             :func:`pcfs_btp<tests.conftest.pcfs_btp()>`
         """
        Vpref = special.pbvv(-pcfs_btp.a-0.5, pcfs_btp.z)[1]
        Vp = pcfs_btp.compute_Vp()
        assert np.isclose(Vpref, Vp, EPS)

    def test_compute_upp_tp(self, pcfs_tp):
        r"""
         :param pcfs_tp: The function fixture --see
             :func:`pcfs_tp<tests.conftest.pcfs_tp()>`
         """
        Uppref = 0.0
        Upp = pcfs_tp.compute_Upp()
        assert np.isclose(Uppref, Upp, 1e-6)

    def test_compute_vpp_tp(self, pcfs_tp):
        r"""
         :param pcfs_tp: The function fixture --see
             :func:`pcfs_tp<tests.conftest.pcfs_tp()>`
         """
        Vppref = 0.0
        Vpp = pcfs_tp.compute_Vpp()
        assert np.isclose(Vppref, Vpp, 1e-5)
