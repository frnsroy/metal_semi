# -*- coding: utf-8 -*-
"""
tests.test_wave.py
January 22, 2019
@author Francois Roy
"""
import pytest
import numpy as np
import scipy.constants as cst
from contact.barrier import Barrier
from contact.wave import Wave
from contact.pcfs import PCFs
from contact import M_E, EPS


# Default values
M = {'mI': M_E,
     'mb': 0.25 * M_E,
     'mII': 0.25 * M_E,
     'mhI': M_E,
     'mhb': 0.25 * M_E,
     'mhII': 0.25 * M_E,
     }


class TestWave:
    r"""Test the Wave class."""
    def test_default_values(self, wave_e):
        r"""

        :param wave_e: The wave fixture --see
            :func:`wave_e<tests.conftest.wave_e()>`
        """
        for key, value in wave_e.m.items():
            assert np.isclose(M[key], value, EPS)

    def test_arguments(self, barrier):
        r"""

        :param barrier: The barrier fixture --see
            :func:`barrier<tests.conftest.barrier()>`
        """
        mb = 0.5 * M_E
        eta = 1.1*barrier.values['eta_ceq']
        w = Wave(eta, barrier, mb=mb)
        assert np.isclose(mb, w.m['mb'], EPS)

    def test_eta_value_band_gap(self, barrier):
        r"""

        :param barrier: The barrier fixture --see
            :func:`barrier<tests.conftest.barrier()>`
        """
        eta = 0.09*barrier.values['eta_ceq']
        with pytest.raises(ValueError) as e:
            Wave(eta, barrier)
        e.match(r'.* band-gap .*')

    def test_eta_value_below(self):
        r""""""
        b = Barrier(Va=-10.0)
        eta = 1.1*b.values['eta_ceq']
        with pytest.raises(ValueError) as e:
            Wave(eta, b)
        e.match(r'.* below .*')

    def test_eta_value_above(self):
        r""""""
        b = Barrier(Va=2.0)
        eta = 1.1*b.values['eta_veq']
        with pytest.raises(ValueError) as e:
            Wave(eta, b)
        e.match(r'.* above .*')

    def test_hole_carriers(self, wave_h):
        r"""

        :param wave_h: The wave fixture --see
            :func:`wave_h<tests.conftest.wave_h()>`
        """
        assert wave_h.carrier == 'h'

    def test_electron_carriers(self, wave_e):
        r"""

        :param wave_e: The wave fixture --see
            :func:`wave_e<tests.conftest.wave_e()>`
        """
        assert wave_e.carrier == 'e'

    def test_electrons_get_m(self, wave_e):
        r"""

        :param wave_e: The wave fixture --see
            :func:`wave_e<tests.conftest.wave_e()>`
        """
        mI = wave_e.get_m('I')
        mb = wave_e.get_m('b')
        mII = wave_e.get_m('II')
        assert np.isclose(M['mI'], mI, EPS)
        assert np.isclose(M['mb'], mb, EPS)
        assert np.isclose(M['mII'], mII, EPS)

    def test_holes_get_m(self, wave_h):
        r"""

        :param wave_h: The wave fixture --see
            :func:`wave_h<tests.conftest.wave_h()>`
        """
        mhI = wave_h.get_m('I')
        mhb = wave_h.get_m('b')
        mhII = wave_h.get_m('II')
        assert np.isclose(M['mhI'], mhI, EPS)
        assert np.isclose(M['mhb'], mhb, EPS)
        assert np.isclose(M['mhII'], mhII, EPS)

    def test_electrons_compute_alpha(self, wave_e):
        r"""

        :param wave_e: The wave fixture --see
            :func:`wave_e<tests.conftest.wave_e()>`
        """
        alphaI = wave_e.compute_alpha('I')
        alphab = wave_e.compute_alpha('b')
        alphaII = wave_e.compute_alpha('II')
        assert np.isclose(7.711589, alphaI, 1e-6)
        assert np.isclose(3.855795, alphab, 1e-6)
        assert np.isclose(3.855795, alphaII, 1e-6)

    def test_holes_compute_alpha(self, wave_h):
        r"""

        :param wave_h: The wave fixture --see
            :func:`wave_h<tests.conftest.wave_h()>`
        """
        alphaI = wave_h.compute_alpha('I')
        alphab = wave_h.compute_alpha('b')
        alphaII = wave_h.compute_alpha('II')
        assert np.isclose(2.438619, alphaI, 1e-6)
        assert np.isclose(1.219309, alphab, 1e-6)
        assert np.isclose(1.219309, alphaII, 1e-6)

    def test_electrons_compute_lambdas(self, wave_e):
        r"""

        :param wave_e: The wave fixture --see
            :func:`wave_e<tests.conftest.wave_e()>`
        """
        lambdas = wave_e.compute_lambdas()
        assert np.isclose(0.1591424, lambdas, 1e-6)

    def test_holes_compute_lambdas(self, wave_h):
        r"""

        :param wave_h: The wave fixture --see
            :func:`wave_h<tests.conftest.wave_h()>`
        """
        lambdas = wave_h.compute_lambdas()
        assert np.isclose(0.5032525, lambdas, 1e-6)

    def test_electrons_compute_kappa(self, wave_e):
        r"""

        :param wave_e: The wave fixture --see
            :func:`wave_e<tests.conftest.wave_e()>`
        """
        kappaI = wave_e.compute_kappa('I')
        kappas = wave_e.compute_kappa('b')
        kappaII = wave_e.compute_kappa('II')
        assert np.isclose(7.505251, kappaI, 1e-6)
        assert np.isclose(0.180063, kappas, 1e-6)
        assert np.isclose(1.131459, kappaII, 1e-6)

    def test_holes_compute_kappa(self, wave_h):
        r"""

        :param wave_h: The wave fixture --see
            :func:`wave_h<tests.conftest.wave_h()>`
        """
        kappaI = wave_h.compute_kappa('I')
        kappas = wave_h.compute_kappa('b')
        kappaII = wave_h.compute_kappa('II')
        assert np.isclose(10.309031, kappaI, 1e-6)
        assert np.isclose(1.264452, kappas, 1e-6)
        assert np.isclose(2.512560, kappaII, 1e-6)

    def test_electrons_compute_gammas(self, wave_e):
        r"""

        :param wave_e: The wave fixture --see
            :func:`wave_e<tests.conftest.wave_e()>`
        """
        Gammab = wave_e.compute_Gammas()[0]
        Gamma0 = wave_e.compute_Gammas()[1]
        assert np.isclose(3.348951, Gammab, 1e-6)
        assert np.isclose(5.553607, Gamma0, 1e-6)

    def test_holes_compute_gammas(self, wave_h):
        r"""

        :param wave_h: The wave fixture --see
            :func:`wave_h<tests.conftest.wave_h()>`
        """
        Gammab = wave_h.compute_Gammas()[0]
        Gamma0 = wave_h.compute_Gammas()[1]
        assert np.isclose(0.7710032, Gammab, 1e-6)
        assert np.isclose(0.7908564, Gamma0, 1e-6)

    def test_set_eta(self, barrier, wave_e):
        r"""

        :param wave_e: The wave fixture --see
            :func:`wave_e<tests.conftest.wave_e()>`
        """
        eta = 1.2*barrier.values['eta_ceq']
        wave_e.set_eta(eta)
        assert np.isclose(eta, wave_e.eta, 1e-6)

    def test_psi(self, wave_e):
        r"""

        :param wave_e: The wave fixture --see
            :func:`wave_e<tests.conftest.wave_e()>`
        """
        xi = 0.1
        xis = (1-xi)/wave_e.values['lambdas']
        a = -wave_e.values['kappas']**2
        psipp = wave_e.compute_psipp('exact', xi)
        psi = wave_e.compute_psi('exact', xi)
        eq_r = psipp['psipp_real'] - (xis**2/4+a)*psi['psi_real']
        eq_i = psipp['psipp_imag'] - (xis**2/4+a)*psi['psi_imag']
        assert np.isclose(0.0, eq_r, EPS)
        assert np.isclose(0.0, eq_i, EPS)

    def test_psi_1(self):
        r""""""
        xi = 0.1
        b = Barrier()
        eta = 1.1*b.values['eta_ceq']
        w = Wave(eta, b)
        a = -w.values['kappas']**2
        z = (1.0-xi)/w.values['lambdas']
        pcf = PCFs(a, z)
        U0 = w.f0.values['U']
        U0p = w.f0.values['Up']
        U = pcf._values['U']
        Upp = pcf.compute_Upp()
        V0 = w.f0.values['V']
        V = pcf._values['V']
        Vpp = pcf.compute_Vpp()
        Br = U0p*np.sqrt(np.pi/2)
        Bi = 1j*w.values['kappaII'] \
               * w.m['mb']*w.values['lambdas'] \
               / w.m['mII']*U0*np.sqrt(np.pi/2)
        realPart = 1/U0*(1+Br*V0)*(Upp-z**2/4*U-a*U)-Br*(Vpp-z**2/4*V-a*V)
        imagPart = 1/U0*(Bi*V0)*(Upp-z**2/4*U-a*U)-Bi*(Vpp-z**2/4*V-a*V)
        assert np.isclose(0.0, realPart, EPS)
        assert np.isclose(0.0, imagPart, EPS)
        assert np.isclose(0.0, Upp-z**2/4*U-a*U, EPS)
        assert np.isclose(0.0, Vpp-z**2/4*V-a*V, EPS)

    def test_betas_alphas(self):
        r""""""
        b = Barrier()
        eta = 1.1*b.values['eta_ceq']
        w = Wave(eta, b)
        U0 = w.f0.values['U']
        U0p = w.f0.values['Up']
        V0 = w.f0.values['V']
        Br = U0p*np.sqrt(np.pi/2)
        Bi = w.values['kappaII']*w.m['mb']*w.values['lambdas'] \
                                / w.m['mII']*U0*np.sqrt(np.pi/2)
        tau = w.compute_tau('exact')
        betas = w.compute_betas('exact')
        B = -betas/tau
        alphas = w.compute_alphas('exact')
        A = alphas/tau
        Ar = 1/U0*(1+Br*V0)
        Ai = 1/U0*(Bi*V0)
        assert np.isclose(Br, B.real, EPS)
        assert np.isclose(Bi, B.imag, EPS)
        assert np.isclose(Ar, A.real, EPS)
        assert np.isclose(Ai, A.imag, EPS)
