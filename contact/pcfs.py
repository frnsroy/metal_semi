# -*- coding: utf-8 -*-
"""
contact.pcfs.py
January 22, 20189
@author Francois Roy
"""
import scipy.special as special
from . import *


class PCFs:
    """Generate the parabolic cylinder functions.

    :param a:
    :param z:
    """
    def __init__(self, a, z, **kwargs):
        # Default values
        inputs = {}
        if kwargs is not None:
            for key, value in kwargs.items():
                if key in inputs:
                    inputs[key] = value
        self._inputs = inputs
        self._a = None
        self.check_a(a)
        self._z = None
        self.check_z(z)
        self._values = {}
        self.init_values()

    @property
    def a(self):
        return self._a

    @a.setter
    def a(self, value):
        self.check_a(value)
        self.init_values()

    @property
    def z(self):
        return self._z

    @z.setter
    def z(self, value):
        self.check_z(value)
        self.init_values()

    @property
    def inputs(self):
        return self._inputs

    @property
    def values(self):
        return self._values

    def init_values(self):
        r""""""
        UU = special.pbdv(-self._a - 0.5, self._z)
        VV = special.pbvv(-self._a - 0.5, self._z)
        U = UU[0]
        Up = UU[1]
        V = VV[0]
        Vp = VV[1]
        self._values = {'U': U,
                        'V': V,
                        'Up': Up,
                        'Vp': Vp,
                        'Uint': self.compute_Uint(),
                        'Vint': self.compute_Vint(),
                        'Upint': self.compute_Upint(),
                        'Vpint': self.compute_Vpint(),
                        }

    def check_a(self, a):
        """Make sure that :math:`a` is real and that :math:`a<=0`.

        :param a:
        """
        if np.iscomplex(a):
            raise ValueError('The value of a must be real.\n')
        if a > 0.0:
            raise ValueError('The value of a must be negative.\n')
        self._a = a

    def check_z(self, z):
        """Make sure that :math:`z` is real and that :math:`z<=0`.

        :param z:
        """
        if np.iscomplex(z):
            raise ValueError('The value of z must be real.\n')
        if z < 0.0:
            raise ValueError('The value of z must be positive.\n')
        self._z = z

    def compute_Up(self):
        U = special.pbdv(-self._a - 0.5, self._z)[0]
        U1 = special.pbdv(-self._a - 0.5 + 1.0, self._z)[0]
        return -U1 + self._z / 2.0 * U

    def compute_Vp(self):
        V = special.pbvv(-self._a - 0.5, self._z)[0]
        V1 = special.pbvv(-self._a - 0.5 + 1.0, self._z)[0]
        Gamma = special.gamma(0.5 - self._a)
        Gamma1 = special.gamma(1.5 - self._a)
        return self._z / 2.0 * V - Gamma1 / Gamma * V1

    def compute_Upp(self):
        U = special.pbdv(-self._a - 0.5, self._z)[0]
        Up = special.pbdv(-self._a - 0.5, self._z)[1]
        U2 = special.pbdv(-self._a - 0.5 + 2.0, self._z)[0]
        return U2 + (0.5 - self._z ** 2 / 4) * U + self._z * Up

    def compute_Vpp(self):
        V = special.pbvv(-self._a - 0.5, self._z)[0]
        Vp = special.pbvv(-self._a - 0.5, self._z)[1]
        V2 = special.pbvv(-self._a - 0.5 + 2.0, self._z)[0]
        Gamma = special.gamma(0.5 - self._a)
        Gamma1 = special.gamma(2.5 - self._a)
        return Gamma1 / Gamma * V2 + (0.5 - self._z ** 2 / 4) * V + self._z * Vp

    def compute_S(self):
        """Compute the action integral"""
        a = self._a
        # Notation from DLMF
        mu = np.sqrt(2.0*abs(a))
        if np.isclose(0.0, mu, EPS):
            t = BIG_NUMBER
        else:
            t = self._z / (np.sqrt(2.0) * mu)
        if t >= 1.0:
            S = mu**2.0*(0.5*t*np.sqrt(t**2-1.0)
                         - 0.5*np.log(t+np.sqrt(t**2-1.0)))
        else:
            S = mu**2.0*(0.5*np.arccos(t)-0.5*t*np.sqrt(1.0-t**2))
        return S

    def compute_Uint(self):
        r""""""
        S = self.compute_S()
        arg0 = self._z ** 2 / 4 - abs(self._a)
        arg1 = special.gamma(0.5 + abs(self._a))
        if np.isclose(0.0, arg0, EPS):
            return special.pbdv(-0.5, self._z)[0]
        arg = (2*np.pi)**0.25*np.sqrt(arg1)/(abs(arg0)**0.25)
        if arg0 >= 0.0:
            Y = (3/2*S)**(2/3)
        else:
            Y = -(3/2*S)**(2/3)
        Ai = special.airy(Y)[0]
        arg2 = (3/2*abs(S))**(1/6)*Ai
        return arg*arg2

    def compute_Vint(self):
        r""""""
        S = self.compute_S()
        arg0 = self._z ** 2 / 4.0 - abs(self._a)
        arg1 = special.gamma(0.5 + abs(self._a))
        if np.isclose(0.0, arg0, EPS):
            return special.pbvv(-0.5, self._z)[0]
        arg = (2*np.pi)**0.25/(np.sqrt(arg1)*abs(arg0)**0.25)
        if arg0 >= 0.0:
            Y = (3/2*S)**(2/3)
        else:
            Y = -(3/2*S)**(2/3)
        Bi = special.airy(Y)[2]
        arg2 = (3/2*S)**(1/6)*Bi
        return arg*arg2

    def compute_Upint(self):
        r""""""
        S = self.compute_S()
        if np.isclose(0.0, S, EPS):
            return special.pbdv(-0.5, self._z)[1]
        arg0 = self._z ** 2 / 4.0 - abs(self._a)
        arg1 = special.gamma(0.5 + abs(self._a))
        arg = (2.0*np.pi)**(0.25)*np.sqrt(arg1)*abs(arg0)**(0.25)
        if arg0 >= 0.0:
            Y = (3/2*S)**(2/3)
        else:
            Y = -(3/2*S)**(2/3)
        Aip = special.airy(Y)[1]
        arg2 = (3/2*S)**(-1/6)*Aip
        return arg*arg2

    def compute_Vpint(self):
        r""""""
        S = self.compute_S()
        if np.isclose(0.0, S, EPS):
            return special.pbvv(-0.5, self._z)[1]
        arg0 = self._z ** 2 / 4.0 - abs(self._a)
        arg1 = special.gamma(0.5 + abs(self._a))
        arg = (2*np.pi*abs(arg0))**0.25/np.sqrt(arg1)
        if arg0 >= 0.0:
            Y = (3/2*S)**(2/3)
        else:
            Y = -(3/2*S)**(2/3)
        Bip = special.airy(Y)[3]
        arg2 = (3/2*S)**(-1/6)*Bip
        return arg*arg2

    @staticmethod
    def compute_pcfs_derivative(fct, a, z):
        r"""Compute the second derivative numerically using the gradient
        function defined in numpy.
        :param fct:
        :param a:
        :param z:
        """
        # if at turning point second derivative must be zero
        if np.isclose(0.0, z):
            x = np.linspace(0.0, 1e-6, 1001)
        else:
            x = np.linspace(0.9*z, 1.1*z, 1001)
        dx = np.gradient(x)
        y = np.zeros(x.size)
        i = 0
        for val in x:
            if fct == 'U':
                y[i] = special.pbdv(-a-0.5, val)[1]
            else:
                y[i] = special.pbvv(-a-0.5, val)[1]
            i += 1
        res = np.gradient(y, dx, edge_order=2)
        return res[500]

    def print_values(self):  # pragma: no cover
        r""""""
        print('VALUES ----------')
        for key, value in self._values.items():
            print('{0:10} ==> {1:=5E}'.format(key, value))
