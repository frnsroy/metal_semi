# -*- coding: utf-8 -*-
"""
contact.__init__.py
January 22, 20189
@author Francois Roy
"""
import numpy as np
import scipy.constants as cst
from pint import UnitRegistry

# TODO: add units to values
ureg = UnitRegistry()
Q_ = ureg.Quantity

# version defined here
__version__ = '0.0.1'

# Constants
BIG_NUMBER = 1E20
EPS = np.finfo(float).eps
EPSILON_0 = cst.epsilon_0
HBAR = cst.hbar
K_B = cst.k
M_E = cst.m_e
Q = cst.e

# Default values
M = {'mI': M_E,
     'mb': 0.25 * M_E,
     'mII': 0.25 * M_E,
     'mhI': M_E,
     'mhb': 0.25 * M_E,
     'mhII': 0.25 * M_E,
     }
DEFAULT_INPUTS = {'Va': 0.0,
                  'Nd': 1e25,
                  'Na': 0.0,
                  'Phi': 4.75,
                  'Eg': 1.12,
                  'chi': 4.05,
                  'T': 300.0,
                  'epsilonrs': 11.7,
                  }
