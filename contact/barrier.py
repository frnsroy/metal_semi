# -*- coding: utf-8 -*-
"""
contact.barrier.py
January 22, 20189
@author Francois Roy
"""
import scipy.integrate as integrate
from . import *


class Barrier:
    r"""Generate a parabolic barrier."""
    def __init__(self, **kwargs):
        # Default values
        inputs = {'Va': 0.0,
                  'Nd': 1e25,
                  'Na': 0.0,
                  'Phi': 4.75,
                  'Eg': 1.12,
                  'chi': 4.05,
                  'T': 300.0,
                  'epsilonrs': 11.7,
                  }
        if kwargs is not None:
            for key, value in kwargs.items():
                if key in inputs:
                    inputs[key] = value
        self._inputs = inputs
        self._values = {}
        self.init_values()

    @property
    def inputs(self):
        r""":return: inputs"""
        return self._inputs

    @property
    def values(self):
        r""":return: values"""
        return self._values

    def compute_gammas(self):
        r"""Determine the degeneracy factors of the semiconductor using the
        Fermi integrals. In the non-degenerate limit, the carrier distribution
        can be approximated by the Maxwell-Boltzmann distribution,
        otherwise the distribution must be calculated using the Fermi-Dirac
        distribution.

        To take account of the degeneracy we define the following factors:

        .. math:: \gamma_n &= \frac{F_{1/2}\left(-(\eta_c-\eta_f)\right)}
            {\exp\left(-(\eta_c-\eta_f)\right)}\\
            \gamma_p &= \frac{F_{1/2}\left(-(\eta_f-\eta_v)\right)}
            {\exp\left(-(\eta_f-\eta_v)\right)}

        where

        .. math:: F_{j}(\eta) = \frac{1}{\Gamma(j+1)}\int_0^\infty
            \frac{\epsilon^j}{1+\exp(\epsilon-\eta)}d\epsilon

        is the Fermi-Dirac integral and where :math:`\Gamma` is the gamma
        function. Note that :math:`\Gamma(3/2)=\sqrt{\pi}/2`.

        :return: The degeneracy factors, gamma_s = [gamma_n, gamma_p].
        """
        inputs = self._inputs
        Nd = inputs['Nd']
        Na = inputs['Na']
        Ndoping = Nd - Na + 1  # 1 is to avoid division by zero
        T = inputs['T']
        Vth = K_B * T / Q
        Nv = (T/300.0)**(3/2)*1.04e25
        Nc = (T/300.0)**(3/2)*2.8e25
        chi = inputs['chi']
        Eg = inputs['Eg']
        ni = np.sqrt(Nc*Nv)*np.exp(-0.5*Eg/Vth)
        # n_eq = ni ** 2 / (
        #             -0.5 * Ndoping + np.sqrt((0.5 * Ndoping) ** 2 + ni ** 2))
        # p_eq = ni ** 2 / (0.5 * Ndoping +
        #                   np.sqrt((0.5 * Ndoping) ** 2 + ni ** 2))
        if Ndoping >= 0:
            n_eq = 0.5*Ndoping+np.sqrt((0.5*Ndoping)**2+ni**2)
            V_eq = -0.5*Eg-chi+Vth*(0.5*np.log(Nv/Nc) +
                                    np.log(n_eq/ni))
        else:
            p_eq = -0.5*Ndoping+np.sqrt((0.5*Ndoping)**2+ni**2)
            V_eq = -0.5*Eg-chi+Vth*(0.5*np.log(Nv/Nc) -
                                    np.log(p_eq/ni))
        Ec_eq = -(V_eq+chi)
        Ev_eq = Ec_eq-Eg
        eta_n = -Ec_eq/Vth
        eta_p = Ev_eq/Vth
        # Integrate the Fermi-Dirac distribution up to eta=500 (integration
        # of the distribution from eta=500 to infinity doesn't have
        # a significant effect on the result.
        F_n = 2/np.pi**0.5*integrate.quad(lambda eta:
                                          eta**0.5/(1+np.exp(eta-eta_n)),
                                          0, 500.0)[0]
        F_p = 2/np.pi**0.5*integrate.quad(lambda eta:
                                          eta**0.5/(1+np.exp(eta-eta_p)),
                                          0, 500.0)[0]
        gamma_n = np.minimum(F_n/np.exp(eta_n), 1.0)
        gamma_p = np.minimum(F_p/np.exp(eta_p), 1.0)
        gammas = [gamma_n, gamma_p]
        return gammas

    def init_values(self):
        r"""Calculate the main semiconductor properties.

        :return: The values.
        """
        inputs = self._inputs
        Nd = inputs['Nd']
        Na = inputs['Na']
        epsilonrs = inputs['epsilonrs']
        T = inputs['T']
        Phi = inputs['Phi']
        chi = inputs['chi']
        Eg = inputs['Eg']
        Va = inputs['Va']
        gammas = self.compute_gammas()
        gamma_n = gammas[0]
        gamma_p = gammas[1]
        Ndoping = Nd - Na + 1  # 1 is to avoid division by zero
        Nv = (T/300.0)**(3/2)*1.04e25
        Nc = (T/300.0)**(3/2)*2.8e25
        PhiB_e = Phi-chi
        PhiB_h = Eg-PhiB_e
        lambda_D = np.sqrt(EPSILON_0 * epsilonrs * K_B * T / (Q ** 2 * np.abs(Ndoping)))
        Vth = K_B * T / Q
        ni = np.sqrt(Nc*Nv)*np.exp(-0.5*Eg/Vth)
        ni_eff = ni*np.sqrt(gamma_n*gamma_p)
        if Ndoping >= 0:
            n_eq = 0.5*Ndoping+np.sqrt((0.5*Ndoping)**2+ni**2)
            p_eq = ni**2/(0.5*Ndoping +
                          np.sqrt((0.5*Ndoping)**2+ni**2))
            V_eq = -0.5*Eg-chi+Vth*(0.5*np.log(Nv/Nc) +
                                    np.log(n_eq/(ni_eff*gamma_n)))
        else:
            n_eq = ni**2/(-0.5*Ndoping+np.sqrt((0.5*Ndoping)**2+ni**2))
            p_eq = -0.5*Ndoping+np.sqrt((0.5*Ndoping)**2+ni**2)
            V_eq = -0.5*Eg-chi+Vth*(0.5*np.log(Nv/Nc) -
                                    np.log(p_eq/(ni_eff*gamma_p)))
        Ec_eq = -(V_eq+chi)
        Ev_eq = Ec_eq-Eg
        Ef_s = 0.0  # Zero Reference
        Vbi = PhiB_e-Ec_eq
        Ef_I = Ef_s-Va
        ubi = Vbi/Vth
        ua = Va/Vth
        eta_ceq = Ec_eq/Vth
        eta_veq = Ev_eq/Vth
        eta_fs = Ef_s/Vth
        eta_g = Eg/Vth
        eta_fI = Ef_I/Vth
        eta_be = PhiB_e/Vth
        eta_bh = PhiB_h/Vth
        if -eta_fI < ubi:
            xb_e = np.sqrt(2 * epsilonrs * EPSILON_0 /
                           (Q * np.abs(Ndoping)) * (ubi + eta_fI) * Vth)
            xb_h = 0.0
        else:
            xb_e = 0.0
            xb_h = np.sqrt(2 * epsilonrs * EPSILON_0 /
                           (Q * np.abs(Ndoping)) * (-eta_fI - ubi) * Vth)
        self._values = {'Ndoping': Ndoping,
                        'Nv': Nv,
                        'Nc': Nc,
                        'PhiB_e': PhiB_e,
                        'PhiB_h': PhiB_h,
                        'lambda_D': lambda_D,
                        'Vth': Vth,
                        'ni': ni,
                        'n_eq': n_eq,
                        'p_eq': p_eq,
                        'V_eq': V_eq,
                        'Ec_eq': Ec_eq,
                        'Ev_eq': Ev_eq,
                        'Ef_s': Ef_s,
                        'Vbi': Vbi,
                        'Ef_I': Ef_I,
                        'ubi': ubi,
                        'ua': ua,
                        'eta_ceq': eta_ceq,
                        'eta_veq': eta_veq,
                        'eta_fs': eta_fs,
                        'eta_g': eta_g,
                        'eta_fI': eta_fI,
                        'eta_be': eta_be,
                        'eta_bh': eta_bh,
                        'xb_e': xb_e,
                        'xb_h': xb_h,
                       }

    def barrier_levels(self, num):
        r"""
        :param num:
        :return: a 3 x num array. The first row contains the normalized
          barrier coordinates, the second contains the bottom of the conduction
          band in the bulk semiconductor and the third the top of the valence
          band in the bulk semiconductor.
        """
        xi = np.linspace(0.0, 1.0, num)
        eta_veq = self._values['eta_veq']
        eta_ceq = self._values['eta_ceq']
        lambda_D = self._values['lambda_D']
        eta_g = self._values['eta_g']
        xb_e = self._values['xb_e']
        xb_h = self._values['xb_h']
        if xb_e == 0.0:
            # -eta_fI >= ubi
            eta_v = eta_veq-xb_h**2/(2*lambda_D**2)*(1-xi)**2
            eta_c = eta_v + eta_g
        else:
            # -eta_fI < ubi
            eta_c = eta_ceq+xb_e**2/(2*lambda_D**2)*(1-xi)**2
            eta_v = eta_c - eta_g
        output = np.array([xi, eta_c, eta_v])
        return output

    def set_applied_voltage(self, Va):
        r"""Set the applied voltage.

        :param Va:
        """
        self._inputs['Va'] = Va
        self.init_values()  # reset initial values

    def set_temperature(self, T):
        r"""Set the lattice temperature.

        :param T: The temperature.
        """
        self._inputs['T'] = T
        self.init_values()  # reset initial values

    def print_properties(self):  # pragma: no cover
        r"""display the barrier properties on the console."""
        print('INPUTS ----------')
        for key, value in self._inputs.items():
            print('{0:10} ==> {1:=5E}'.format(key, value))
        print('VALUES ----------')
        for key, value in self._values.items():
            print('{0:10} ==> {1:=5E}'.format(key, value))
