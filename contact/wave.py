# -*- coding: utf-8 -*-
"""
contact.wave.py
January 22, 20189
@author Francois Roy
"""
from .pcfs import PCFs
from . import *


class Wave:
    r"""Generate the wave parameters.

    :param eta:
    :param b:
    """
    def __init__(self, eta, b, **kwargs):
        # Default values
        m = {'mI': M_E,
             'mb': 0.25 * M_E,
             'mII': 0.25 * M_E,
             'mhI': M_E,
             'mhb': 0.25 * M_E,
             'mhII': 0.25 * M_E,
             }
        if kwargs is not None:
            for key, value in kwargs.items():
                if key in m:
                    m[key] = value
        self.m = m
        if (eta < b.values['eta_ceq'] and
           eta > b.values['eta_veq']):
            raise ValueError('The normalized energy is in the '
                             'band-gap of the semiconductor.\n')
        self.b = b
        self.check_eta_is_valid(eta)
        self.init_values()

    def check_eta_is_valid(self, eta):
        r""""""
        if self.b.values['xb_e'] > 0.0:
            self.carrier = 'e'
            if (eta-self.b.values['eta_fI']) < 0.0:
                raise ValueError('The normalized energy is '
                                 'below the metal Fermi level.')
        else:
            self.carrier = 'h'
            if (eta-self.b.values['eta_fI']) > 0.0:
                raise ValueError('The normalized energy is '
                                 'above the metal Fermi level.')
        self.eta = eta

    def init_values(self):
        r""""""
        alphaI = self.compute_alpha('I')
        alphab = self.compute_alpha('b')
        alphaII = self.compute_alpha('II')
        lambdas = self.compute_lambdas()
        xisb = 1.0/lambdas
        kappaI = self.compute_kappa('I')
        kappas = self.compute_kappa('b')
        kappaII = self.compute_kappa('II')
        Gammab = self.compute_Gammas()[0]
        Gamma0 = self.compute_Gammas()[1]
        a = -kappas**2
        self.f0 = PCFs(a, 0)
        self.fb = PCFs(a, xisb)
        eta_min = np.maximum(self.b.values['eta_ceq'], self.b.values['eta_fI'])
        eta_max = np.minimum(self.b.values['eta_veq'], self.b.values['eta_fI'])
        self.values = {'alphaI': alphaI,
                       'alphab': alphab,
                       'alphaII': alphaII,
                       'lambdas': lambdas,
                       'xisb': xisb,
                       'kappaI': kappaI,
                       'kappas': kappas,
                       'kappaII': kappaII,
                       'Gammab': Gammab,
                       'Gamma0': Gamma0,
                       'eta_min': eta_min,
                       'eta_max': eta_max,
                       }

    def get_m(self, region):
        r""""""
        if self.carrier == 'e':
            if region == 'I':
                m = self.m['mI']
            elif region == 'b':
                m = self.m['mb']
            else:
                m = self.m['mII']
        else:
            if region == 'I':
                m = self.m['mhI']
            elif region == 'b':
                m = self.m['mhb']
            else:
                m = self.m['mhII']
        return m

    def compute_alpha(self, region):
        r""""""
        m = self.get_m(region)
        T = self.b.inputs['T']
        if self.carrier == 'e':
            xb = self.b.values['xb_e']
        else:
            xb = self.b.values['xb_h']
        alpha = np.sqrt(2 * m * K_B * T) * xb / HBAR
        return alpha

    def compute_lambdas(self):
        r""""""
        lambda_D = self.b.values['lambda_D']
        if self.carrier == 'e':
            xb = self.b.values['xb_e']
        else:
            xb = self.b.values['xb_h']
        alpha = self.compute_alpha('b')
        lambdas = np.sqrt(lambda_D/(np.sqrt(2)*alpha*xb))
        return lambdas

    def compute_kappa(self, region):
        r""""""
        # kappa is always real, i.e. eta > eta_min
        if region == 'I':
            if self.carrier == 'e':
                delta_eta = self.eta-self.b.values['eta_fI']
            else:
                delta_eta = -(self.eta-self.b.values['eta_fI'])
        else:
            if self.carrier == 'e':
                delta_eta = self.eta-self.b.values['eta_ceq']
            else:
                delta_eta = -(self.eta-self.b.values['eta_veq'])
        alpha = self.compute_alpha(region)
        lambdas = self.compute_lambdas()
        if region == 'b':
            kappa = np.sqrt(delta_eta)*alpha*lambdas
        else:
            kappa = np.sqrt(delta_eta)*alpha
        return kappa

    def compute_Gammas(self):
        r""""""
        mI = self.get_m('I')
        mb = self.get_m('b')
        mII = self.get_m('II')
        kappaI = self.compute_kappa('I')
        kappaII = self.compute_kappa('II')
        lambdas = self.compute_lambdas()
        if np.isclose(0.0, lambdas, EPS):
            return BIG_NUMBER
        if np.isclose(0.0, kappaI, EPS):
            Gammab = BIG_NUMBER
        else:
            Gammab = mI/(kappaI*mb*lambdas)
        if np.isclose(0.0, kappaII, EPS):
            Gamma0 = BIG_NUMBER
        else:
            Gamma0 = mII/(kappaII*mb*lambdas)
        return [Gammab, Gamma0]

    def set_eta(self, eta):
        r""""""
        self.check_eta_is_valid(eta)
        self.init_values()

    def set_b(self, b):
        r""""""
        self.b = b
        self.init_values()

    def compute_A(self, method):
        r""""""
        if method == 'exact':
            A = self.fb._values['U'] * self.f0._values['Vp'] - \
                self.f0._values['Up'] * self.fb._values['V']
        else:
            A = self.fb._values['Uint'] * self.f0._values['Vpint'] - \
                self.f0._values['Upint'] * self.fb._values['Vint']
        return A

    def compute_B(self, method):
        r""""""
        if method == 'exact':
            B = self.f0._values['U'] * self.fb._values['Vp'] - \
                self.fb._values['Up'] * self.f0._values['V']
        else:
            B = self.f0._values['Uint'] * self.fb._values['Vpint'] - \
                self.fb._values['Upint'] * self.f0._values['Vint']
        return B

    def compute_C(self, method):
        r""""""
        if method == 'exact':
            C = self.fb._values['Up'] * self.f0._values['Vp'] - \
                self.f0._values['Up'] * self.fb._values['Vp']
        else:
            C = self.fb._values['Upint'] * self.f0._values['Vpint'] - \
                self.f0._values['Upint'] * self.fb._values['Vpint']
        return C

    def compute_D(self, method):
        r""""""
        if method == 'exact':
            D = self.fb._values['U'] * self.f0._values['V'] - \
                self.f0._values['U'] * self.fb._values['V']
        else:
            D = self.fb._values['Uint'] * self.f0._values['Vint'] - \
                self.f0._values['Uint'] * self.fb._values['Vint']
        return D

    def compute_TCoef(self, method):
        r""""""
        A = self.compute_A(method)
        B = self.compute_B(method)
        C = self.compute_C(method)
        D = self.compute_D(method)
        mI = self.get_m('I')
        mb = self.get_m('b')
        mII = self.get_m('II')
        kappaI = self.values['kappaI']
        kappaII = self.values['kappaII']
        lambdas = self.values['lambdas']
        if np.isclose(0.0, kappaI, EPS) or\
           np.isclose(0.0, kappaII, EPS) or\
           np.isclose(0.0, lambdas, EPS):
            return 0.0
        g = np.pi/4*(mII*kappaI/(mI*kappaII)*A**2 +
                     mI*kappaII/(mII*kappaI)*B**2 +
                     mI*mII/(kappaI*kappaII*mb**2*lambdas**2)*C**2 +
                     kappaI*kappaII*mb**2*lambdas**2/(mI*mII)*D**2)
        return 2/(g+1)

    def compute_tau(self, method):
        r""""""
        a0 = 1.0
        Gammab = -1j*self.values['Gammab']
        Gamma0 = -1j*self.values['Gamma0']
        if method == 'exact':
            Ub = self.fb._values['U']
            Ubp = self.fb._values['Up']
            U0 = self.f0._values['U']
            U0p = self.f0._values['Up']
            Vb = self.fb._values['V']
            Vbp = self.fb._values['Vp']
            V0 = self.f0._values['V']
            V0p = self.f0._values['Vp']
        denom = (Ub-Gammab*Ubp)*(V0/Gamma0+V0p) - \
                (Vb-Gammab*Vbp)*(U0/Gamma0+U0p)
        return a0*np.sqrt(8/np.pi)/denom

    def compute_alphas(self, method):
        r""""""
        tau = self.compute_tau(method)
        betas = self.compute_betas(method)
        if method == 'exact':
            U0 = self.f0._values['U']
            V0 = self.f0._values['V']
        return (tau-betas*V0)/U0

    def compute_betas(self, method):
        r""""""
        Gamma0 = -1j*self.values['Gamma0']
        tau = self.compute_tau(method)
        if method == 'exact':
            U0 = self.f0._values['U']
            U0p = self.f0._values['Up']
        return -tau*np.sqrt(np.pi/2)*(U0p+U0/Gamma0)

    def compute_psi(self, method, xi):
        r""""""
        lambdas = self.values['lambdas']
        xis = (1-xi)/lambdas
        a = -self.values['kappas']**2
        alphas = self.compute_alphas(method)
        betas = self.compute_betas(method)
        U = PCFs(a, xis)._values['U']
        V = PCFs(a, xis)._values['V']
        psi = alphas*U + betas*V
        psi_real = psi.real
        psi_imag = psi.imag
        psi_norm = np.sqrt(np.absolute(psi))
        return {"psi_real": psi_real, "psi_imag": psi_imag,
                "psi_norm": psi_norm}

    def compute_psi_figure(self, method, xi_v):
        r""""""
        # Make sure 1D array with values in the range [0.0, 1.0]
        lambdas = self.values['lambdas']
        xis = (1-xi_v)/lambdas
        a = -self.values['kappas']**2
        alphas = self.compute_alphas(method)
        betas = self.compute_betas(method)
        psi = np.zeros((2, xis.size))
        i = 0
        for z in xis:
            U = PCFs(a, z)._values['U']
            V = PCFs(a, z)._values['V']
            psi[0][i] = (alphas*U + betas*V).real
            psi[1][i] = (alphas*U + betas*V).imag
            i += 1
        return psi

    def compute_psipp(self, method, xi):
        r""""""
        lambdas = self.values['lambdas']
        xis = (1-xi)/lambdas
        a = -self.values['kappas']**2
        alphas = self.compute_alphas(method)
        betas = self.compute_betas(method)
        Upp = PCFs(a, xis).compute_Upp()
        Vpp = PCFs(a, xis).compute_Vpp()
        psipp = alphas*Upp + betas*Vpp
        psipp_real = psipp.real
        psipp_imag = psipp.imag
        psipp_norm = np.sqrt(np.absolute(psipp))
        return {"psipp_real": psipp_real, "psipp_imag": psipp_imag,
                "psipp_norm": psipp_norm}

    def print_properties(self):  # pragma: no cover
        r"""Display the wave properties on the console."""
        print('MASSES ----------')
        for key, value in self.m.items():
            print('{0:10} ==> {1:=5E}'.format(key, value))
        print('VALUES ----------')
        for key, value in self.values.items():
            print('{0:10} ==> {1:=5E}'.format(key, value))
