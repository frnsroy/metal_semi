===========================
Metal-Semiconductor Contact
===========================
.. image:: https://codeship.com/projects/dd38ea60-7a64-0134-555b-7662665ae19d/status?branch=master
    :target: https://codeship.com/projects/180724
.. image:: https://codecov.io/bb/frnsroy/metal-semi/coverage.svg?branch=master
    :target: https://codecov.io/bb/frnsroy/metal-semi

- `Build and test history <https://codeship.com/projects/180724>`_
- Licensed under BSD-2


A basic code to get the the quantum tunneling current in a metal-semiconductor contact.

Usage
-----

Create a virtual environment then:

- pip install --upgrade wheel setuptools pip
- pip install -r requirements.txt
- pip install -e .
