#!/usr/bin/env python
# -*- coding: utf-8 -*-

try:
    from setuptools import setup
except ImportError:
    print("Couldn't import setuptools. Falling back to distutils.")
    from distutils.core import setup
from distutils.util import convert_path


main_ns = {}
ver_path = convert_path('contact/__init__.py')
with open(ver_path) as ver_file:
    exec(ver_file.read(), main_ns)

setup(
    name='metal-semi',
    version=main_ns['__version__'],
    author='Francois Roy',
    author_email='francois@froy.ca',
    description=("Quantum Tunneling in a Metal-Semiconductor Contact."),
    license='BSD-2',
    keywords="Schrodinger equation",
    url='https://bitbucket.org/frnsroy/metal-semi',
    packages=['contact', 'tests'],
    package_dir={'metal-semi':
                 'contact'},
    include_package_data=True,
    setup_requires=['pytest-runner', ],
    test_suite='tests.test_class',
    tests_require=['pytest', ],
    zip_safe=False,
    classifiers=[
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.5',
    ],
    )
